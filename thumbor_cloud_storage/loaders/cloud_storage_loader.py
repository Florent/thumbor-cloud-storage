from tornado.concurrent import return_future
from gcloud import storage
from collections import defaultdict

buckets = defaultdict(dict)

@return_future
def load(context, path, callback):
    logger.error("start retrieve")
    bucket_id  = context.config.get("CLOUD_STORAGE_BUCKET_ID")
    project_id = context.config.get("CLOUD_STORAGE_PROJECT_ID")
    logger.error("bucket id %s" % bucket_id)
    logger.error("project id %s" % project_id)
    logger.error("bucket id %s" % path)
    bucket = buckets[project_id].get(bucket_id, None)
    if bucket is None:
        logger.error("dansle bucket none")
        client = storage.Client(project_id)
        bucket = client.get_bucket(bucket_id)
        buckets[project_id][bucket_id] = bucket

    blob = bucket.get_blob(path)
    if blob:
        logger.error("recup blob ok")
        callback(blob.download_as_string())
    else:
        logger.error("recup blob nok")
        callback(blob)
